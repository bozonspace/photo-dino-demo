import React from "react";
import CityItem from "./CityItem";

const Cities = ({
  cities,
  deleteCity,
  setUpdatingCity,
  fetchLocations,
  setCity,
  setAddCity,
  setSearchCity,
  searchCity,
}) => {
  return (
    <div className='cities d-flex flex-column py-1 m-3 mt-5 py-lg-5 m-lg-5 container max-w-576 rounded-20 border border-gray-ced4da'>
      <div className='d-flex flex-row justify-content-between pt-2 pt-sm-0 px-4 px-sm-7'>
        <h1 className="text-center">Cities</h1>
        {searchCity.searching && (
          <button className='btn btn-danger' style={{ height: "40px" }}>
            <i
              className='fa fa-times'
              onClick={() => {
                setSearchCity({
                  ...searchCity,
                  searching: false,
                  text: "",
                  cities: [],
                });
              }}
            ></i>
          </button>
        )}
      </div>

      {searchCity.searching && searchCity.cities.length === 0 ? (
        <p className='text-center'>No record found</p>
      ) : (
        cities.map((city) => (
          <CityItem
            key={city.id}
            city={city}
            deleteCity={deleteCity}
            setUpdatingCity={setUpdatingCity}
            fetchLocations={fetchLocations}
            setCity={setCity}
            setAddCity={setAddCity}
          />
        ))
      )}
    </div>
  );
};

export default Cities;
