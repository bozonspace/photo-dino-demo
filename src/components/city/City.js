import React from "react";
import LocationItem from "../location/LocationItem";

const City = ({
  city: { name, code },
  locations,
  setCity,
  setLocations,
  setUpdateLocation,
  setAddLocation,
  deleteLocation,
  fetchEvents,
  setLocation,
}) => {
  return (
    <div className='back'>
      <div className='city d-flex flex-column my-2 my-lg-5 bg-white mx-1 container max-w-576 p-3 mw-sm-auto overflow-auto '>
        <button
          className='btn btn-sm btn-danger'
          onClick={() => {
            setCity({});
            setLocations([]);
            setAddLocation(false);
            setUpdateLocation(false);
          }}
        >
          <i className='fa fa-times'></i>
        </button>
        <h1>{name}</h1>
        <h2>{"- code: " + code}</h2>
        <h3>Places to visit</h3>
        <button
          className='btn btn-primary'
          onClick={() => {
            setAddLocation(true);
            setUpdateLocation({});
          }}
        >
          <i className='fa fa-plus'></i> Add a location
        </button>
        {locations.map((location) => (
          <LocationItem
            location={location}
            setUpdateLocation={setUpdateLocation}
            deleteLocation={deleteLocation}
            setAddLocation={setAddLocation}
            fetchEvents={fetchEvents}
            key={location.id}
            setCity={setCity}
            setLocation={setLocation}
          />
        ))}
      </div>
    </div>
  );
};

export default City;
