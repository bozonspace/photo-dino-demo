import React from "react";

const EventButtons = ({
  setView, deleteEvent, id, event, setUpdateEvent, setAddEvent, 
}) => {
  return (
    <div style={{ marginLeft: "auto" }}>
      <button
        className='btn btn-outline-warning btn-sm'
        style={{ width: "40px" }}
        onClick={() => {
          setUpdateEvent(event);
          setAddEvent(false);
        }}
      >
        <i className='fa fa-edit'></i>
      </button>
      <button
        className='btn btn-outline-danger btn-sm mx-2'
        style={{ width: "40px" }}
        onClick={() => deleteEvent(id)}
      >
        <i className='fa fa-trash'></i>
      </button>
      <button
        className='btn btn-outline-secondary btn-sm'
        style={{ width: "40px" }}
        onClick={() => setView(false)}
      >
        <i className='fa fa-angle-up'></i>
      </button>
    </div>
  );
};

export default EventButtons;
