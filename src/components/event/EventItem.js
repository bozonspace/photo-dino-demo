import React, { useState } from "react";
import EventButtons from "./EventButtons";

const EventItem = ({
  event,
  deleteEvent,
  setUpdateEvent,
  setAddEvent,
}) => {
  const {name, event_date, status, description, id} = event;
  const [view, setView] = useState(false);
  const fullDate = new Date(event_date);
  const month = fullDate.toLocaleString('en-GB', { month: 'long' });
  const date = fullDate.getDate();
  const year = fullDate.getFullYear();
  return (
    <div className='location-item justify-content-between align-items-end'>
      <div className='d-flex flex-column' style={{ width: "100%" }}>
        <h4>{name}</h4>
        <Available status={status}/>
        <h5 className="">{`Date: ${month} ${date}, ${year}`}</h5>
        {view ? <h5>{description}</h5> : ''} 
        {view && <EventButtons
          setView={setView}
          deleteEvent={deleteEvent}
          id={id}
          event={event}
          setUpdateEvent={setUpdateEvent}
          setAddEvent={setAddEvent}
          />} 
      </div>
      {
        !view && <button
        className='btn btn-outline-primary btn-sm'
        onClick={() => { setView(true); }}
      >
        <i className='fa fa-eye'></i>
      </button>
      }
    </div>
  );
};

const Available = ({status}) => {
  return (
    <>
			{status === "Available" && (
				<h6>
					<i className='fa fa-circle' style={{ color: "green" }}></i>{" "}
					Available
				</h6>
			)}

			{status === "Cancelled" && (
				<h6>
					<i className='fa fa-circle' style={{ color: "red" }}></i>{" "}
					Cancelled
				</h6>
			)}

			{status === "Scheduled" && (
				<h6>
					<i className='fa fa-circle' style={{ color: "yellow" }}></i> Scheduled
				</h6>
			)}

			{status === "Complete" && (
				<h6>
					<i className='fa fa-circle' style={{ color: "orange" }}></i> Complete
				</h6>
			)}
    </>
  );
}

export default EventItem;
