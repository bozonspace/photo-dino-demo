import React, { useState, useEffect } from "react";

const EventUpdate = ({
  updateEvent,
  updatingEvent,
  setUpdateEvent,
}) => {
  const [formData, setFormData] = useState({
    event_date: '0000-00-00',
    status: "Available",
    name: "",
    description: '',
  });

  useEffect(() => {
    setFormData({
      name: updateEvent.name ? updateEvent.name : "",
      status: updateEvent.status ? updateEvent.status : "Available",
      event_date: updateEvent.event_date ? updateEvent.event_date : '0000-00-00',
      description: updateEvent.description ? updateEvent.description : '',
    });
  }, [updateEvent]);

  const {
    event_date,
    status,
    name,
    description,
  } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    if (!name) {
      alert("Please add a name");
      return;
    }

    if (!description) {
      alert("Please add a description");
      return;
    }

    if (!event_date) {
      alert("Please add an event date");
      return;
    }

    if (!status) {
      alert("Please add a status");
      return;
    }
    setUpdateEvent({});
    updatingEvent(formData, updateEvent.id);
  };

  return (
  <div className="px-3 my-5">
    <div className='p-4 d-flex flex-column container max-w-576 rounded-20 border border-gray-ced4da'>
        <div className='d-flex justify-content-between'>
          <h2>Update Event</h2>
          <button
            className='btn btn-danger'
            onClick={() => setUpdateEvent(false)}
          >
            <i className='fa fa-times'></i>
          </button>
        </div>

        <form onSubmit={(e) => onSubmit(e)}>
          <div className='mb-3 d-flex flex-column'>
            <label htmlFor='Name' className='form-label'>
              Name
            </label>
            <input
              type='text'
              className='form-control'
              id='Name'
              placeholder='Name'
              name='name'
              value={name}
              onChange={(e) => onChange(e)}
            />
          </div>

          <div className='mb-3 d-flex flex-column'>
            <label htmlFor='Name' className='form-label'>
              Description
            </label>
            <textarea
              type='text'
              className='form-control'
              id='Description'
              placeholder='Description'
              name='description'
              value={description}
              onChange={(e) => onChange(e)}
            />
          </div>
        
          <div className='mb-3 d-flex flex-column'>
            <label htmlFor='status' className='form-label'>
              Status
            </label>
            <select
              className='form-select'
              aria-label='Default select example'
              id='status'
              name='status'
              value={status}
              onChange={(e) => onChange(e)}
            >
              <option value='Scheduled'>Scheduled</option>
              <option value='Cancelled'>Cancelled</option>
              <option value='Available'>Available</option>
              <option value='Complete'>Complete</option>
            </select>
          </div>

          <div className='mb-3 d-flex flex-column'>
            <label htmlFor='status' className='form-label'>
              Date
            </label>
            <input
              type='date'
              className='form-control'
              id='Date'
              placeholder='Date'
              name='event_date'
              value={event_date}
              onChange={(e) => onChange(e)}
            />
          </div>
          <button type='submit' className='btn btn-outline-primary'>
            Submit
          </button>
        </form>
    </div>
  </div>
    
  );
};

export default EventUpdate;
