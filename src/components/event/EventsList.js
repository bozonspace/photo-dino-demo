import React from "react";
import EventItem from "./EventItem";

const EventsList = ({
  location,
  events,
  city,
  setLocation,
  deleteEvent,
  setAddEvent,
  setUpdateEvent,
}) => {
  return (
    <div className='back'>
      <div className='city d-flex flex-column my-2 my-lg-5 bg-white mx-1 container p-3 mw-sm-auto overflow-auto'>
        <button
          className='btn btn-sm btn-danger'
          onClick={() => {
            setLocation({});
          }}
        >
          <i className='fa fa-times'></i>
        </button>
        <h1>{location.name}</h1>
        <h2>{`${location.street_name}, ${location.street_number} (${city.name})`}</h2>
        <h3>Events to visit</h3>
        <button
          className='btn btn-primary'
          onClick={() => {
            setAddEvent(true)
            // setUpdateLocation({});
          }}
        >
          <i className='fa fa-plus'></i> Add an event
        </button>
        {events.map((event) => (
          <EventItem 
            event={event}
            key={event.id}
            deleteEvent={deleteEvent}
            setUpdateEvent={setUpdateEvent}
            setAddEvent={setAddEvent}
          />
        ))}
      </div>
    </div>
  );
};

export default EventsList;
